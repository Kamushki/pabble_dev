<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent {

	public function trans()
	{
		return $this->hasMany('Trans', 'user_id', 'id');
	}

	public function car()
	{
		return $this->hasOne('Car');
	}

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'auth_hash');

	/**
	 * @param null $hash
	 */
	public function setHash($hash = null)
	{
		$this->auth_hash = md5(md5($this->id."".uniqid())."!@#$%^&*()_+ k,sd'");
	}
}
