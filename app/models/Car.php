<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Car extends Eloquent {

	public function user()
	{
		return $this->hasMany('Users');
	}

	protected $table = 'cars';

}
