<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class CarMigration {
    function run()
    {
        Capsule::schema()->dropIfExists('cars');
        Capsule::schema()->create('cars', function($table) {
            $table->increments('id');
            $table->integer('level');
            $table->string('url');
            $table->timestamps();
        });
    }
}
