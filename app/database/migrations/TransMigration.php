<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class TransMigration {
    function run()
    {
        Capsule::schema()->dropIfExists('trans');
        Capsule::schema()->create('trans', function($table) {
            $table->increments('id');
            $table->integer('amount');
            $table->integer('user_id');
            $table->timestamps();
        });
    }
}
