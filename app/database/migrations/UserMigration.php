<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class UserMigration {
    function run()
    {
        Capsule::schema()->dropIfExists('users');
        Capsule::schema()->create('users', function($table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->string('auth_hash');
            $table->integer('group');
            $table->string('profile_image');
            $table->integer('car');
            $table->integer('xp');
            $table->timestamps();
        });
    }
}
