<?php

class UserSeed
{

    function run()
    {
        $names = [
            "Вихорева Яна", "Лапухова Евдокия", "Крутелева Оксана", "Курдина Милена", "Каштанова Софья", "Аникин Федот", "Завразина Лилия", "Коллерова Светлана", "Головкин Роман", "Тёмкина Полина", "Лощилов Феликс", "Ясногородский Кир", "Балунов Клавдий", "Денисова Анастасия", "Ростова Каролина", "Погребной Римма", "Яимов Марк", "Вышегородских Артемий", "Кондакова Эльвира", "Ширманов Руслан", "Цулукидзе Зиновий", "Цорна Милена", "Серыха Инна", "Жичкин Кирилл", "Коллерова Лариса", "Трапезников Порфирий", "Оболенская Светлана", "Цейдлица Инесса", "Пятосин Якуб", "Пономарева Юлия", "Щеглов Владлен", "Сияновича Доминика", "Грибоедова Рената", "Хемлин Ян", "Борщёва Бронислава", "Кирсанов Иван", "Штельмах Глеб", "Дежнёва Любовь", "Камалова Ефросинья", "Игошев Олег", "Званцов Мартын"];

        for ($i = 0; $i < 20; $i++) {
            $user = new User;
            $user->name = $names[rand(0, count($names)-1)];
            $user->email = "ru" . $i . "@ru.ru";
            $user->password = "123";
            $user->group = 1;
            $user->profile_image = "";
            $user->car = rand(1, 5);
            $user->save();
            for($j = 0; $j < rand(1,30); $j++) {
                $trans = new Trans;
                $trans->amount = rand(500, 5000);
                $trans->user_id = $user->id;
                $trans->save();
            }
        }

        for ($i = 20; $i < 30; $i++) {
            $user = new User;
            $user->name = $names[rand(0, count($names)-1)];
            $user->email = "ru" . $i . "@ru.ru";
            $user->password = "123";
            $user->group = 2;
            $user->profile_image = "";
            $user->car = rand(1, 5);
            $user->save();
            for($j = 0; $j < rand(1,10); $j++) {
                $trans = new Trans;
                $trans->amount = rand(5000, 55000);
                $trans->user_id = $user->id;
                $trans->save();
            }
        }

        for ($i = 30; $i < 40; $i++) {
            $user->name = $names[rand(0, count($names)-1)];
            $user->email = "ru" . $i . "@ru.ru";
            $user->password = "123";
            $user->group = 3;
            $user->profile_image = "";
            $user->car = rand(1, 5);
            $user->save();
            for($j = 0; $j < rand(1,5); $j++) {
                $trans = new Trans;
                $trans->amount = rand(150000, 555000);
                $trans->user_id = $user->id;
                $trans->save();
            }
        }

    }
}
