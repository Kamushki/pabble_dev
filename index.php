<?php

require __DIR__ . '/vendor/autoload.php';

$app = new Slim\Slim();

$app->get('/', function() {
    $user = new User;
    $user->username = "World";
    // $user->save();

    echo "Hello, $user->username!";
});

$app->post('/auth', function () use ($app) {

    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    try {

        $email = $app->request->post('email');
        $password = $app->request->post('password');

        $isUser = User::where('email', $email)
            ->where('password', $password)
            ->first();

        if (!$isUser) throw new Exception('Логин или пароль неверные!');

        $isUser->setHash();
        $isUser->save();

        $result = $isUser->toArray();
        
        $app->response()->status(200);
        $app->response()->header('Content-Type', 'application/json');
        
        echo json_encode($result);
    } catch (Exception $e) {
        $app->response()->status(400);
        echo json_encode(array('status' => false, 'error' => array('code' => 400, 'msg' => $e->getMessage())));
    }

});

$app->post('/logout/:hash', function ($hash) use ($app) {

    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    try {

        $isUser = User::where('auth_hash', $hash)
            ->first();


        if (!$isUser) throw new Exception();

        $isUser->hash = null;
        $isUser->save();


        $app->response()->status(200);
        $app->response()->header('Content-Type', 'application/json');

        echo json_encode(['status'  =>  true]);
    } catch (Exception $e) {
        $app->response()->status(400);
        echo json_encode(array('status' => false, 'error' => array('code' => 400, 'msg' => $e->getMessage())));
    }

});


$app->get('/profile/:hash', function ($hash) use ($app) {
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    try {
        $user = User::where('auth_hash', $hash)
//            ->with(['trans'])
            ->first();

        if (!$user) throw new Exception('Вы не авторизованы, попробуйте войти снова');

        $result = $user->toArray();

        $app->response()->status(200);
        $app->response()->header('Content-Type', 'application/json');
        echo json_encode([
            'status' => true,
            'result' => $result
        ]);

    } catch (Exception $e) {
        $app->response()->status(400);
        echo json_encode(array(
            'status' => false,
            'error' => array('code' => 400, 'msg' => $e->getMessage())));
    }
});

$app->put('/trans/create/:user_id', function ($user_id) use ($app) {
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    try {
        $trans = new Trans();
        $trans->amount = rand(500, 5000);
        $trans->user_id = $user_id;
        $trans->save();


        if (!$trans) throw new Exception('');

        $result = $trans->toArray();

        $app->response()->status(200);
        $app->response()->header('Content-Type', 'application/json');
        echo json_encode([
            'status' => true,
            'result' => $result
        ]);

    } catch (Exception $e) {
        $app->response()->status(400);
        echo json_encode(array(
            'status' => false,
            'error' => array('code' => 400, 'msg' => $e->getMessage())));
    }
});



$app->get('/users/:group', function ($group) use ($app) {
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    try {
        $today = date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d"), date("Y")));
        $users = User::where('group', $group)
            ->get();

        if (!$users) throw new Exception('Пользователи не найдены');

        $result = $users->toArray();

        foreach ($result as $key => $user) {
            $result[$key]['trans_count'] = Trans::where('user_id', $user['id'])
                ->where('created_at','like',"%".$today."%")
                ->count();
        }


        $app->response()->status(200);
        $app->response()->header('Content-Type', 'application/json');
        echo json_encode([
            'status' => true,
            'result' => $result
        ]);

    } catch (Exception $e) {
        $app->response()->status(400);
        echo json_encode(array(
            'status' => false,
            'error' => array('code' => 400, 'msg' => $e->getMessage())));
    }
});

$app->run();
